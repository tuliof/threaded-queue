# Threaded Queue #

Esse projeto é parte da implementação do fluxo de requisição e notificação entre clientes e fornecedores. O fluxo completo incluí a parte que gera e responde à requisição, partes que não estão inclusas neste projeto.

## Instalação ##

Este projeto é baseado no Gradle e já está configurado para suportar as IDEs Eclipse e Idea. Basta rodar o comando para que os arquivos sejam gerados para a sua IDE de sua escolha.

`gradle eclipse` ou `gradle idea`