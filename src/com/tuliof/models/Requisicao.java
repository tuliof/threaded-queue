package com.tuliof.models;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

/**
 * 
 * @author Túlio F.
 *
 */
public class Requisicao {

	public enum RaioDaBusca {
		CURTO, MEDIO, LONGO
	};

	public static final Map<RaioDaBusca, Integer> MapRaioDaBuscaEmKm = criaNovoMap();
	private static Map<RaioDaBusca, Integer> criaNovoMap() {
		Map<RaioDaBusca, Integer> map = new HashMap<>();
		map.put(RaioDaBusca.CURTO, 10);
		map.put(RaioDaBusca.MEDIO, 20);
		map.put(RaioDaBusca.LONGO, 35);
		return map;
	}

	private long id;
	private Cliente cliente;
	private Queue<Estabelecimento> estabelecimentos;

	// Data da reserva
	private LocalDateTime data;

	// Localização do cliente
	private double longitude;
	private double latitude;

	private RaioDaBusca raioDaBusca;

	private int numeroDeTentativas = 0;

	public Requisicao() {
		

		this.raioDaBusca = RaioDaBusca.CURTO;
	}

	/**
	 * @return the estabelecimento
	 */
	public Queue<Estabelecimento> getEstabelecimentos() {
		if (estabelecimentos == null)
			estabelecimentos = new ArrayDeque<Estabelecimento>();

		return estabelecimentos;
	}

	/**
	 * @param estabelecimentos
	 *            the estabelecimento to set
	 */
	public void setEstabelecimento(Queue<Estabelecimento> estabelecimentos) {
		if (estabelecimentos == null)
			throw new InvalidParameterException();

		this.estabelecimentos = estabelecimentos;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the data
	 */
	public LocalDateTime getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(LocalDateTime data) {
		this.data = data;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return o raio da busca em km
	 */
	public int getRaioDaBuscaEmKm() {
		return MapRaioDaBuscaEmKm.get(raioDaBusca);
	}

	/**
	 * @return the numeroDeTentativas
	 */
	public int getNumeroDeTentativas() {
		return numeroDeTentativas;
	}

	/**
	 * @param numeroDeTentativas
	 *            the numeroDeTentativas to set
	 */
	public void incrementarNumeroDeTentativas() {
		this.numeroDeTentativas++;
	}

	public void decrementarNumeroDeTentativas(int numeroDeTentativas) {
		if (this.numeroDeTentativas > 0)
			this.numeroDeTentativas--;
	}

	// TODO: Melhorar isso aqui D:
	public void aumentaRaioDeBusca() {
		//raioDaBusca = MapRaioDaBuscaEmKm.get(numeroDeTentativas);
		
		if (raioDaBusca == RaioDaBusca.CURTO) {
			raioDaBusca = RaioDaBusca.MEDIO;
		} else if (raioDaBusca == RaioDaBusca.MEDIO) {
			raioDaBusca = RaioDaBusca.LONGO;
		}
	}

	/**
	 * @return the raioDaBusca
	 */
	public RaioDaBusca getRaioDaBusca() {
		return raioDaBusca;
	}

	/**
	 * @param raioDaBusca
	 *            the raioDaBusca to set
	 */
	public void setRaioDaBusca(RaioDaBusca raioDaBusca) {
		this.raioDaBusca = raioDaBusca;
	}
}
