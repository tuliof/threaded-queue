package com.tuliof.models;

import java.util.List;

/**
 * 
 * @author Túlio F.
 *
 */
public class Estabelecimento {

	public enum TipoEstabelecimentoEnum {
		CABELEIREIRO, MANICURE, MEDICO, SALAO
	}

	private long id;
	private String nome;
	private Endereco endereco;
	private List<Integer> listaDeTelefones;
	private TipoEstabelecimentoEnum tipoEstabelecimento;

	/**
	 * @return the tipoEstabelecimento
	 */
	public TipoEstabelecimentoEnum getTipoEstabelecimento() {
		return tipoEstabelecimento;
	}

	/**
	 * @param tipoEstabelecimento
	 *            the tipoEstabelecimento to set
	 */
	public void setTipoEstabelecimento(TipoEstabelecimentoEnum tipoEstabelecimento) {
		this.tipoEstabelecimento = tipoEstabelecimento;
	}

	/**
	 * @return the listaDeTelefones
	 */
	public List<Integer> getListaDeTelefones() {
		return listaDeTelefones;
	}

	/**
	 * @param listaDeTelefones
	 *            the listaDeTelefones to set
	 */
	public void setListaDeTelefones(List<Integer> listaDeTelefones) {
		this.listaDeTelefones = listaDeTelefones;
	}

	/**
	 * @return the endereco
	 */
	public Endereco getEndereco() {
		return endereco;
	}

	/**
	 * @param endereco
	 *            the endereco to set
	 */
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

}
