package com.tuliof.Busca;

import java.util.concurrent.BlockingQueue;

import com.tuliof.models.Requisicao;

public abstract class ProcessadorDeFila implements Runnable {
	protected BlockingQueue<Requisicao> filaDeBusca = null;
	protected BlockingQueue<Requisicao> filaDeNotificacao = null;

	/**
	 * Método genérico para pegar o primeiro item de uma BlockingList
	 * 
	 * @param fila
	 *            Instância de BlockingQueue
	 * @return Primeiro item da fila ou null quando a fila está vazia.
	 */
	public <U> U obtemPrimeiroDaFila(BlockingQueue<U> fila) {
		U element = null;

		if (fila.size() > 0) {
			try {
				// Retrieves and removes the head of this queue,
				// **waiting if necessary until an element becomes available**.
				element = (U) fila.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return element;
	}
}
