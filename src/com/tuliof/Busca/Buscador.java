package com.tuliof.Busca;

import java.util.Queue;
import java.util.concurrent.BlockingQueue;

import com.tuliof.models.Estabelecimento;
import com.tuliof.models.Requisicao;
import com.tuliof.models.Requisicao.RaioDaBusca;

/**
 * 
 * @author Túlio F.
 *
 */
public class Buscador extends ProcessadorDeFila {

	protected final int NUMERO_MAXIMO_DE_TENTATIVAS = Requisicao.MapRaioDaBuscaEmKm.size();
	
	public Buscador(BlockingQueue<Requisicao> filaDeBusca, 
			BlockingQueue<Requisicao> filaDeNotificacao) {
		this.filaDeBusca = filaDeBusca;
		this.filaDeNotificacao = filaDeNotificacao;
	}

	@Override
	public void run() {
		busca(this.filaDeBusca, this.filaDeNotificacao);
	}

	/**
	 * 
	 * Pega a primeira requisição da fila e faz a busca de estabelecimentos para essa
	 * requisição.
	 * 
	 * @param filaDeBusca
	 * @param filaDeNotificacao
	 */
	public void busca(BlockingQueue<Requisicao> filaDeBusca, 
			BlockingQueue<Requisicao> filaDeNotificacao) {

		Requisicao req = obtemPrimeiroDaFila(filaDeBusca);

		do {
			adquireEstabelecimentos(req);
			
			if (req.getEstabelecimentos().isEmpty()) {
				req.incrementarNumeroDeTentativas();
				req.aumentaRaioDeBusca();
			} else {
				// Adiciona requisição na fila de notificação e sai do loop
				filaDeNotificacao.offer(req);
				break;				
			}
		} while(req.getNumeroDeTentativas() < NUMERO_MAXIMO_DE_TENTATIVAS);
		
		// Informa o cliente caso nenhum estabelecimento seja encontrado em sua área.
		if (req.getEstabelecimentos().isEmpty()) {
			// TODO: Envia push notification informando o cliente.
			// Ver https://developers.google.com/cloud-messaging/downstream
		}
	}

	/**
	 * Faz a busca geografica e ordena de acordo com as regras.
	 * 
	 * @param requisicao
	 */
	public void adquireEstabelecimentos(Requisicao requisicao) {

		// Busca os estabelecimentos
		Queue<Estabelecimento> estabelecimentos = 
				GeoBusca.EncontraEstabelecimentosNaArea(requisicao.getLatitude(),
				requisicao.getLongitude(), requisicao.getRaioDaBuscaEmKm());

		// Cria o ranking dos estabelecimentos
		Rank.OrdenaEstabelecimentos(estabelecimentos);
		requisicao.setEstabelecimento(estabelecimentos);
	}
}
