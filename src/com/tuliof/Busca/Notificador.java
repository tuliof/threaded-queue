package com.tuliof.Busca;

import java.util.concurrent.BlockingQueue;

import com.tuliof.models.Estabelecimento;
import com.tuliof.models.Requisicao;

public class Notificador extends ProcessadorDeFila {

	public Notificador(BlockingQueue<Requisicao> filaDeNotificacao) {
		this.filaDeNotificacao = filaDeNotificacao;
	}

	@Override
	public void run() {
		notifica(this.filaDeNotificacao);
	}

	/**
	 * Ainda falta um método que controle a resposta da requisição.
	 * Deve poder receber o SIM ou NÃO do estabelecimento:
	 *  Caso SIM: Envia mensagem de confirmação para o cliente.
	 *  Caso NÃO: Verifica se há estabelecimentos na fila da Requisição:
	 *  	Caso SIM: Notifica o próximo.
	 *  	Caso NÃO: Adiciona a requisição na filaDeBusca
	 */
	public void notifica(BlockingQueue<Requisicao> filaDeNotificacao) {
		Requisicao req = obtemPrimeiroDaFila(filaDeNotificacao);

		if (req != null) {
			Estabelecimento est = req.getEstabelecimentos().poll();

			if (est != null) {
				enviaPullRequestParaApp(req, est);
			}
		}

		// Caso a lista esteja zerada, busca por novos estabelecimentos?
		// Só se a requisição for negada, não faz sentido já buscar, será
		// desperdicio caso a req seja aceita
		if (req.getEstabelecimentos().size() == 0) {
			System.out.println("Requisição está com lista de estabelecimentos vazia.");
		}
	}

	public void enviaPullRequestParaApp(Requisicao req, Estabelecimento est) {
		// Aqui você envia a pull request para o estabelecimento escolhido.
	}
}
