package com.tuliof.Busca;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.tuliof.models.Requisicao;

/**
 * 
 *         
 * @author Túlio F.
 * 
 * Baseado em
 * http://tutorials.jenkov.com/java-util-concurrent/blockingqueue.html
 * 
 */
public class Diretor {
	protected BlockingQueue<Requisicao> filaDeNotificacao = null;
	protected BlockingQueue<Requisicao> filaDeBusca = null;

	public Diretor(int tamanoMaximoDaFila) {
		this.filaDeNotificacao = new ArrayBlockingQueue<>(tamanoMaximoDaFila);
		this.filaDeBusca = new ArrayBlockingQueue<>(tamanoMaximoDaFila);

		Buscador buscador = new Buscador(filaDeBusca, filaDeNotificacao);
		Notificador notificador = new Notificador(filaDeNotificacao);

		new Thread(buscador).start();
		new Thread(notificador).start();
	}
}
